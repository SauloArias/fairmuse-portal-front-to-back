# FairMusE Portal WordPress PlugIn

To update plugin on server :
* open terminal on frontend server through SSH
* go to the plugin folder
    `cd /srv/www/portal.fairmuse.eu/https/wp-content/plugins/fairmuse-portal`
* pull code from git repo by executing following command 
    `sudo -u www-data git pull`