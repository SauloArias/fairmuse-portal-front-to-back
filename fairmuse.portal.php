<?php
/*
 * Plugin Name:       FairMusE Portal Wordpress plugin
 * Version:           2.0.0
 * Requires at least: 6.4
 * Requires PHP:      8.1
 * Author:            Saulo Arias Hernandez, Eric Arrivé
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

require_once('backend/Backend.php');

if (!function_exists('write_log')) {
    
    function write_log($log) {
        if (true === WP_DEBUG) {
            if ($log instanceof Exception) {
                error_log($log->__toString());
            } else if (is_array($log) || is_object($log)) {
                error_log(json_encode($log, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
            } else {
                error_log($log);
            }
        }
    }
    
}

// check backend data for music profile page
add_action('init', 'check_backend_donations', 10);
function check_backend_donations() {
    foreach (['REQUEST_SCHEME','SERVER_NAME','REQUEST_URI'] as $key) {
        if (!isset($_SERVER[$key])) {
            return;
        }
    }
    $baseURL  = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'];
    $relevant = TRP_Translate_Press::get_trp_instance()->get_component('url_converter')->get_url_for_language(trp_get_user_language(wp_get_current_user()->ID), $baseURL.'/music-profile/', '');
    $request  = $baseURL.$_SERVER['REQUEST_URI'];
    if ($request !== $relevant) {
        return;
    }
    try {
        get_data_from_backend(wp_get_current_user());
    } catch(BackendException $exception) {
        write_log($exception);
    }
}

//implementing log out redirect

add_action('check_admin_referer', 'pfwp_logout_without_confirm', 10, 2);
function pfwp_logout_without_confirm($action, $result)
{
    if ($action == "log-out" && !isset($_GET['_wpnonce'])){
        $redirect_to = isset($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : get_permalink(142);
        $location = str_replace('&amp;', '&', wp_logout_url($redirect_to));
        header("Location: $location");
        die;
    }
}

// implementing push to the back end for U of Lille

add_action( 'elementor_pro/forms/new_record', 'push_data_to_backend', 10, 2);

//function to send data
function push_data_to_backend( $record, $handler ) {
    
    // Make sure the form is the one you want to target
    if ( $record->get_form_settings( 'form_name' ) !== 'Music Data Uploads' ) {
        return;
    }
    
    // Get the data from the music_upload field
    $fields = $record->get_field( ['id' => 'music_upload'] );
    if (!empty($fields)) {
        $music_upload_field = current($fields);
        if (!empty($music_upload_field) && $music_upload_field['type'] === 'upload') {
            //upload data to backend
            foreach (explode(',', $music_upload_field['raw_value']) as $index => $localPath) {
                try {
                    upload_data_to_backend(wp_get_current_user(), trim($localPath), $index + 1);
                } catch (BackendException $error) {
                    $handler->add_error_message($error->getMessage());
                }
            }
        }
    }
    
}

// implementing push of Listenbrainz data to U of Lille using form from "Frontend Admin" plugin
add_filter('acf/update_value', 'push_listenbrainz_to_backend', 10, 4);

//function to send listenbrainz data
function push_listenbrainz_to_backend( $value, $post_id, $field, $original ){
    // Make sure the field is the one you want to target
    if ($field['name'] === 'listenbrainz_username') {

        $localPath = ABSPATH.'wp-content/uploads/listenbrainz_'.$value.'.zip';
        //Initialize zip archive instance
        $zip = new ZipArchive();
        //Open zip archive
        if ($zip->open($localPath, ZipArchive::CREATE) === true) {
            //Add txt file to zip
            $zip->addFromString("listenbrainz.txt" , $value);
            //Close the zip archive
            if ($zip->close()) {
                try {
                    upload_data_to_backend(wp_get_current_user(), $localPath);
                } catch (BackendException $error) {
                    error_log($error->getMessage());
                }
            }
        }
    }
    //returning value to ensure its saved
    return $value;
}

//implementing reminder email system
add_action('elementor_pro/forms/new_record' , 'schedule_reminder_email' , 10, 2);

//function to schedule the email
function schedule_reminder_email($record, $handler) {
    
    // Make sure the form is the one you want to target
    if ($record->get_form_settings('form_name') !== 'Reminders'){
        return;
    }
    
    //Get the data from the form
    $reminder_email_fields = $record->get_field(['id' => 'reminder_email']);
    $streaming_platform_fields = $record->get_field(['id' => 'streaming_platform']);
    
    //Ensure all fields are present
	if (empty($reminder_email_fields) || empty($streaming_platform_fields)){
        return;
    }
    
    $reminder_email = current($reminder_email_fields)['value'];
    $user = get_user_by('email', $reminder_email);
    if ($user === false) {
        return false;
    }
    $streaming_platform = current($streaming_platform_fields)['value'];
    
    date_default_timezone_set('Europe/Copenhagen');
    
    //Calculate the timestamp based on streaming_platform option
    switch ($streaming_platform) {
        case 'Spotify':
            $reminder_delay = '+4 weeks';
            break;
            
        case 'Apple Music':
            $reminder_delay = '+1 week';
            break;
            
        case 'YouTube Music':
            $reminder_delay = '+1 week';
            break;
            
        default:
            //Default to two weeks if the option is not recognized
            $reminder_delay = '+2 weeks';
            break;
    }
    
    $reminder_language = trp_get_user_language(wp_get_current_user()->ID);
    
    // Schedule the reminder email using WordPress Cron
    wp_schedule_single_event(strtotime($reminder_delay), 'send_reminder_email_event', array($reminder_email, $reminder_language));
    
}

//Hook into the scheduled event to send the reminder email
add_action('send_reminder_email_event', 'send_reminder_email', 10, 2);

//Function to send the reminder email
function send_reminder_email($reminder_email, $reminder_language) {
    //Set the content type to HTML
    $headers = array(
        'From' => 'webmaster@its.aau.dk',
        'Reply-To' => 'portal@fairmuse.eu',
        'Xmailer' => 'PHP/' . phpversion(),
        'Content-Type' => 'text/html; charset=UTF-8'
    );
    $subject = trp_translate('FairMusE Portal - Reminder to Share Data', $reminder_language);
    $message = trp_translate(file_get_contents(dirname(__FILE__).'/mail.reminder.html'), $reminder_language);
    //Use PHP mail to send the email
    mail($reminder_email, $subject, $message, $headers);
    
}

add_action('admin_menu', array('Backend', 'addOptions'));

//function to upload data to backend
function upload_data_to_backend($user, $localPath, $index = 1) {
    return (new Backend())->upload($user, $localPath, $index);
}

//function to get data from backend
function get_data_from_backend($user) {
    return (new Backend())->donations($user);
}

function get_donor_info() {
    return json_decode(get_user_meta(wp_get_current_user()->ID, 'backend_donations', true));
}

//enabling ACF shortcodes
add_action( 'acf/init', 'enable_acf_shortcode' );
function enable_acf_shortcode() {
    acf_update_setting( 'enable_shortcode', true );
}

?>
