<div class="wrap">
<h1>Backend Settings</h1>
<form method="post" action="options.php"> 
<?php settings_fields( 'backend-settings-group' ); ?>
<?php do_settings_sections( 'backend-settings-group' ); ?>
    <table class="form-table">
<?php foreach(Backend::getOptions() as $name => $config) { ?>
        <tr valign="top">
            <th scope="row"><?php echo $config['description'] ?? $name; ?></th>
            <td><input type="<?php echo $config['input'] ?? 'text'; ?>" name="<?php echo 'backend_'.$name; ?>" value="<?php echo esc_attr( get_option('backend_'.$name) ); ?>" /></td>
        </tr>
<?php } ?>
    </table>
    <?php submit_button(); ?>
</form>
</div>
