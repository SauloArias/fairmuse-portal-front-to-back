<?php 

class Backend {
    
    private $host;
    private $port;
    private $user;
    private $public;
    private $private;
    private $passphrase;
    private $password;
    private $folder;
    private $connection = null;
    private $sftp       = null;
    
    public static function getOptions() {
        return json_decode(file_get_contents(dirname(__FILE__) . '/settings.json'), true);
    }
    
    public static function addOptions() {
        add_options_page(
            'Backend Options',
            'Backend',
            'manage_options',
            'options-backend',
            function() {
                include('settings.php');
            }
            );
        foreach (Backend::getOptions() as $name => $config) {
            register_setting( 'backend-settings-group', 'backend_'.$name, $config);
        }
    }
    
    public function __construct() {
        foreach (self::getOptions() as $name => $config) {
            $this->$name = get_option('backend_'.$name, $config['default']);
        }
        $this->connection = ssh2_connect($this->host, $this->port);
        if (!empty($this->password)) {
            ssh2_auth_password($this->connection, $this->user, $this->password);
        } else {
            ssh2_auth_pubkey_file($this->connection, $this->user, $this->realPath($this->public), $this->realPath($this->private), !empty($this->passphrase) ? $this->passphrase : null);
        }
        $this->sftp = ssh2_sftp($this->connection);
    }
    
    private function realpath($path) {
        if (substr($path, 0, 1) === '~') {
            $home = $_SERVER['HOME'] ?? null;
            if (empty($home)) {
                $home = getenv("HOME");
            }
            if (empty($home)) {
                $home = posix_getpwuid(getmyuid())['dir'] ?? null;
            }
            $path = str_replace('~', $home, $path);
        }
        return realpath($path);
    }
    
    // based on https://www.php.net/manual/fr/function.ssh2-exec.php#125100
    private function exec($command, $log = null) {
        $result = false;
        $out = '';
        $err = '';
        $sshout = ssh2_exec($this->connection, $command);
        if ($sshout) {
            $ssherr = ssh2_fetch_stream($sshout, SSH2_STREAM_STDERR);
            if ($ssherr) {
                # we cannot use stream_select() with SSH2 streams
                # so use non-blocking stream_get_contents() and usleep()
                if (stream_set_blocking($sshout, false) && stream_set_blocking($ssherr, false)) {
                    $result = true;
                    # loop until end of output on both stdout and stderr
                    $wait = 0;
                    while (!feof($sshout) or !feof($ssherr)) {
                        # sleep only after not reading any data
                        if ($wait) {
                            usleep($wait);
                        }
                        $wait = 50000; # 1/20 second
                        foreach (['OUT' => $sshout, 'ERR' => $ssherr] as $target => $stream) {
                            if (!feof($stream)) {
                                $content = stream_get_contents($stream);
                                if ($content === false) {
                                    $result = false;
                                    break;
                                }
                                if ($content !== '') {
                                    if ($log) {
                                        foreach (explode("\n", $content) as $line) {
                                            if (!empty(trim($line))) {
                                                call_user_func($log, $command, $target, $line);
                                            }
                                        }
                                    }
                                    switch ($target) {
                                        case 'OUT': {
                                            $out .= $content;
                                            break;
                                        }
                                        case 'ERR': {
                                            $err .= $content;
                                            break;
                                        }
                                    }
                                    $wait = 0;
                                }
                            }
                            if ($result === false) {
                                break;
                            }
                        }
                    }
                }
                # we need to wait for end of command
                stream_set_blocking($sshout, true);
                stream_set_blocking($ssherr, true);
                # these will not get any output
                stream_get_contents($sshout);
                stream_get_contents($ssherr);
                fclose($ssherr);
            }
            fclose($sshout);
        }
        return [$result, $out, $err];
    }
    
    private function recv($source, $target) {
        return $this->copy($source, $target, 'pull');
    }
    
    private function send($source, $target) {
        return $this->copy($source, $target, 'push');
    }
    
    private function copy($source, $target, $direction) {
        return $this->handleError([
            'try' => function() use($source, $target, $direction) {
                switch($direction) {
                    case 'pull': {
                        if (!file_exists(dirname($target))) {
                            mkdir(dirname($target), 0777, true);
                        }
                        return ssh2_scp_recv($this->connection, $source, $target);
                    }
                    case 'push': {
                        ssh2_sftp_mkdir($this->sftp, dirname($target), 0777, true);
                        return ssh2_scp_send($this->connection, $source, $target);
                    }
                }
            },
            'catch' => function($exception) {
                return false;
            }
        ]);
    }
    
    private function handleError($parameters) {
        set_error_handler(function($errno, $errstr, $errfile, $errline, $errcontext) {
            if (0 === error_reporting()) {
                return false;
            }
            throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
        });
            try {
                if (isset($parameters['try']) && is_callable($parameters['try'])) {
                    return call_user_func($parameters['try']);
                }
            } catch (ErrorException $exception) {
                if (isset($parameters['catch']) && is_callable($parameters['catch'])) {
                    return call_user_func($parameters['catch'], $exception);
                }
            } finally {
                if (isset($parameters['finally']) && is_callable($parameters['finally'])) {
                    call_user_func($parameters['finally']);
                }
                restore_error_handler();
            }
    }
    
    private function execute($command, $parameters) {
        $output = null;
        if (!is_array($parameters)) {
            $parameters = [$parameters];
        }
        if (!preg_match('/^[a-z]+$/', $command)) {
            throw new BackendException("Forbidden command : ".$command, 403);
        }
        foreach ($parameters as $parameter) {
            if (!is_scalar($parameter) || !preg_match('/^(\w|-|\.)+$/', $parameter)) {
                throw new BackendException("Forbidden parameter : ".$parameter, 403);
            }
        }
        $task = $command.' '.implode(' ', $parameters);
        if (!$this->exec('cd fairmuse; flask backend '.$task, function($command, $target, $line) use (&$output) {
            if ($target === 'OUT') {
                $output .= $line."\n";
            }
        })) {
            throw new BackendException("Execution error while running : ".$task, 500);
        }
        try {
            return json_decode($output, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $error) {
            throw new BackendException("Unserialization error with output : ".$output, 500, $error);
        }
    }
    
    private function uuid($user) {
        //Save this UUID in relation with donor profile
        //That will be the key to integrate data vizualisation in the portal
        $unique_id = get_user_meta($user->ID, 'backend_uuid', true);
        if (empty($unique_id)) {
            $unique_id = wp_generate_uuid4();
            if (add_user_meta($user->ID, 'backend_uuid', $unique_id, true) === false) {
                throw new BackendException('Failed to add backend_uuid as user metadata');
            }
        }
        return $unique_id;
    }
    
    public function upload($user, $localPath, $index) {
        
        $unique_id = $this->uuid($user);
        
        $previous = get_user_meta($user->ID, 'backend_uploads', true);
        $uploads = json_decode($previous, true);
        if (!is_array($uploads)) {
            $uploads = [];
            $previous = json_encode($uploads, JSON_PRETTY_PRINT);
            if (add_user_meta($user->ID, 'backend_uploads', $previous, true) === false) {
                throw new BackendException('Failed to add backend_uploads as user metadata');
            }
        }
        
        //Preparing data using acf field names
        $country = get_user_meta($user->ID, 'country_of_residence' , true);
        $gender  = get_user_meta($user->ID, 'gender' , true);
        $age     = get_user_meta($user->ID, 'age' , true);
        if ($gender === 'other') {
            $gender = get_user_meta($user->ID, 'gender_other' , true);
        }
        $user_data = array(
            'id'           => $unique_id,
            'country'      => $country,
            'gender'       => $gender,
            'age'          => $age,
        );
        
        //Convert data to JSON format
        $json_data = json_encode($user_data, JSON_PRETTY_PRINT);
        
        //define remote path for uploaded file
        $remotePath = $this->folder. '/import' . date_i18n('YmdHis') . str_pad('' . $index, 4, '0', STR_PAD_LEFT) . '.zip';
        
        //Initialize zip archive instance
        $zip = new ZipArchive();
        //Open zip archive
        if ($zip->open($localPath) === true) {
            //Add json file to zip
            $zip->addFromString("donor.json" , $json_data);
            //Close the zip archive
            if ($zip->close()) {
                //Upload enhanced zip to backend
                if ($this->send($localPath, $remotePath) === false) {
                    // throw exception if upload failed
                    throw new BackendException('Failed to upload file ' . $localPath . 'to remote server.');
                } else {
                    $uploads[$localPath] = $remotePath;
                }
            }
        } else {
            //Handle zip file enhancement failure
            throw new BackendException('Failed to add donor.json to file ' . $localPath);
        }
        
        //Close backend connection
        $this->exec('exit');
        
        if (update_user_meta($user->ID, 'backend_uploads', json_encode($uploads, JSON_PRETTY_PRINT), $previous) === false) {
            throw new BackendException('Failed to update backend_uploads as user metadata');
        }
        
    }
    
    public function donations($user) {
        $backend_uuid = $this->uuid($user);
        $previous = get_user_meta($user->ID, 'backend_donations', true);
        $donations = json_encode($this->execute('donations', [$backend_uuid]), JSON_PRETTY_PRINT);
        $store = true;
        if ($previous === '') {
            $store = add_user_meta($user->ID, 'backend_donations', $donations, true);
        } else if ($donations !== $previous) {
            $store = update_user_meta($user->ID, 'backend_donations', $donations, $previous);
        }
        if ($store === false) {
            throw new BackendException('Failed to store backend_donations as user metadata');
        }
    }
    
}

class BackendException extends Exception {
    
}

?>