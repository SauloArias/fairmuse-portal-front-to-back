<?php 

function contains($file) {
    $folder = dirname(__FILE__);
    while (!file_exists($folder.'/'.$file)) {
        $folder = dirname($folder);
    }
    return $folder.'/'.$file;
}

require_once contains('wp-config.php');

try {
    $files = glob(contains('uploads').'/elementor/forms/*.zip');
    shuffle($files);
    $user = get_user_by('login', $_SERVER['argv'][1]);
    echo json_encode(get_data_from_backend($user), JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES)."\n";
    upload_data_to_backend($user, $files[0]);
} catch (BackendException $error) {
    echo $error->getMessage()."\n";
}

?>
